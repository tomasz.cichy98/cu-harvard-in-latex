% Encoding: UTF-8
@article{Vinyals2019,
	abstract = {Many real-world applications require artificial agents to compete and coordinate with other agents in complex environments. As a stepping stone to this goal, the domain of StarCraft has emerged as an important challenge for artificial intelligence research, owing to its iconic and enduring status among the most difficult professional esports and its relevance to the real world in terms of its raw complexity and multi-agent challenges. Over the course of a decade and numerous competitions1--3, the strongest agents have simplified important aspects of the game, utilized superhuman capabilities, or employed hand-crafted sub-systems4. Despite these advantages, no previous agent has come close to matching the overall skill of top StarCraft players. We chose to address the challenge of StarCraft using general-purpose learning methods that are in principle applicable to other complex domains: a multi-agent reinforcement learning algorithm that uses data from both human and agent games within a diverse league of continually adapting strategies and counter-strategies, each represented by deep neural networks5,6. We evaluated our agent, AlphaStar, in the full game of StarCraft II, through a series of online games against human players. AlphaStar was rated at Grandmaster level for all three StarCraft races and above 99.8\% of officially ranked human players.},
	author = {Oriol Vinyals and Igor Babuschkin and Wojciech M. Czarnecki and Micha{\"e}l Mathieu and Andrew Dudzik and Junyoung Chung and David H. Choi and Richard Powell and Timo Ewalds and Petko Georgiev and Junhyuk Oh and Dan Horgan and Manuel Kroiss and Ivo Danihelka and Aja Huang and Laurent Sifre and Trevor Cai and John P. Agapiou and Max Jaderberg and Alexander S. Vezhnevets and R{\'e}mi Leblond and Tobias Pohlen and Valentin Dalibard and David Budden and Yury Sulsky and James Molloy and Tom L. Paine and Caglar Gulcehre and Ziyu Wang and Tobias Pfaff and Yuhuai Wu and Roman Ring and Dani Yogatama and Dario W{\"u}nsch and Katrina McKinney and Oliver Smith and Tom Schaul and Timothy Lillicrap and Koray Kavukcuoglu and Demis Hassabis and Chris Apps and David Silver},
	doi = {10.1038/s41586-019-1724-z},
	issn = {1476-4687},
	journal = {Nature},
	number = {7782},
	pages = {350--354},
	risfield_0_da = {2019/11/01},
	title = {Grandmaster level in StarCraft II using multi-agent reinforcement learning},
	volume = {575},
	year = {2019}
}

@article{article,
	author = {Peter Adams},
	journal = {The name of the journal},
	month = 7,
	note = {An optional note},
	number = 2,
	pages = {201--213},
	title = {The title of the work},
	volume = 4,
	year = 1993
}

@book{book,
	address = {The address},
	author = {Peter Babington},
	edition = 3,
	isbn = {3257227892},
	month = 7,
	note = {An optional note},
	publisher = {The name of the publisher},
	series = 10,
	title = {The title of the work},
	volume = 4,
	year = 1993
}

@booklet{booklet,
	address = {The address of the publisher},
	author = {Peter Caxton},
	howpublished = {How it was published},
	month = 7,
	note = {An optional note},
	title = {The title of the work},
	year = 1993
}

@conference{conference,
	address = {The address of the publisher},
	author = {Peter Draper},
	booktitle = {The title of the book},
	editor = {The editor},
	month = 7,
	note = {An optional note},
	organization = {The organization},
	pages = 213,
	publisher = {The publisher},
	series = 5,
	title = {The title of the work},
	volume = 4,
	year = 1993
}

@inbook{inbook,
	address = {The address of the publisher},
	author = {Peter Eston},
	chapter = 8,
	edition = 3,
	month = 7,
	note = {An optional note},
	pages = {201--213},
	publisher = {The name of the publisher},
	series = 5,
	title = {The title of the work},
	volume = 4,
	year = 1993
}

@incollection{incollection,
	address = {The address of the publisher},
	author = {Peter Farindon},
	booktitle = {The title of the book},
	chapter = 8,
	edition = 3,
	editor = {The editor},
	month = 7,
	note = {An optional note},
	pages = {201--213},
	publisher = {The name of the publisher},
	series = 5,
	title = {The title of the work},
	volume = 4,
	year = 1993
}

@manual{manual,
	address = {The address of the publisher},
	author = {Peter Gainsford},
	edition = 3,
	month = 7,
	note = {An optional note},
	organization = {The organization},
	title = {The title of the work},
	year = 1993
}

@mastersthesis{mastersthesis,
	address = {The address of the publisher},
	author = {Peter Harwood},
	month = 7,
	note = {An optional note},
	school = {The school of the thesis},
	title = {The title of the work},
	year = 1993
}

@misc{misc,
	author = {Peter Isley},
	howpublished = {How it was published},
	month = 7,
	note = {An optional note},
	title = {The title of the work},
	year = 1993
}

@phdthesis{phdthesis,
	address = {The address of the publisher},
	author = {Peter Joslin},
	month = 7,
	note = {An optional note},
	school = {The school of the thesis},
	title = {The title of the work},
	year = 1993
}

@proceedings{proceedings,
	address = {The address of the publisher},
	editor = {Peter Kidwelly},
	month = 7,
	note = {An optional note},
	organization = {The organization},
	publisher = {The name of the publisher},
	series = 5,
	title = {The title of the work},
	volume = 4,
	year = 1993
}

@techreport{techreport,
	address = {The address of the publisher},
	author = {Peter Lambert},
	institution = {The institution that published},
	month = 7,
	note = {An optional note},
	number = 2,
	title = {The title of the work},
	year = 1993
}

@unpublished{unpublished,
	author = {Peter Marcheford},
	month = 7,
	note = {An optional note},
	title = {The title of the work},
	year = 1993
}

@misc{website,
	access_date = {access date},
	author = {Chris Xeener},
	journal = {Website Name},
	month = {Dec},
	publisher = {Website Name},
	title = {Website title},
	url = {www.example.com},
	year = {2016}
}

@Comment{jabref-meta: databaseType:bibtex;}

@Comment{jabref-meta: fileDirectoryLatex-tomasz-tomasz:/home/tomasz/Documents/projects/cu-harvard-in-latex/example;}
