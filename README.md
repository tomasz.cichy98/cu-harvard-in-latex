# CU Harvard In LaTeX
[ ![cu_harvard](cu_harvard.png "CU Harvard, see example") ](./example)


[[_TOC_]]

## About the project
Coventry University requires its students to use a custom referencing style called CU Harvard. There is currently no way to use CU Harvard in LaTeX and many which might be a big problem for many students. I have decided to create a custom bibliography style for LaTeX and share it with other students.  
I don't think I will be able to make it 100% compliant with the CU Harvard Referencing guide because of the number of different types of references specified in the guide. I am going to stylise all BibTeX entry types to match the referencing guide starting from the most popular ones.  

**Disclaimer:**  
The referencing style is subjectively constructed and I take no responsibility for errors made in the design (provided under [MIT license](./LICENSE)). Please consult the quality of references with your supervisor before submitting. You can report errors and ask for specific features in the [issues tab](https://gitlab.com/tomasz.cichy98/cu-harvard-in-latex/-/issues) or [issue board tab](https://gitlab.com/tomasz.cichy98/cu-harvard-in-latex/-/boards).

## How to install (works with [Overleaf](https://www.overleaf.com/project/5c0986643b5200372463ddeb), `.bst` file not up to date!)
1. Download the [cuharvard.bst](./bst/cuharvard.bst) file
2. Put it in the same folder as your .tex file

## How to use (see [example](./example/)), to learn about custom entries used in this referencing style see the [Wiki page](https://gitlab.com/tomasz.cichy98/cu-harvard-in-latex/-/wikis/Custom-BibTex-entries)
1. Install the `.bst` file
1. In LaTeX preamble
    ```latex
    \documentclass{article}
    \usepackage[utf8]{inputenc}
    \usepackage[british]{babel}
    \usepackage[document]{ragged2e}
    \usepackage{csquotes}               % single quotes
    \usepackage{url}                    % support urls
    \usepackage{natbib}                 % Harvard refs
    \usepackage[T1]{fontenc}            % correct character encodings
    \usepackage{lmodern}                % a font
    \usepackage{pifont}                 % http://ctan.org/pkg/pifont

    \setcitestyle{aysep={},notesep={:~}}% in text style
    ```
1. Store references in a `.bib` file (see [How to use BibTex](https://www.overleaf.com/learn/latex/bibliography_management_with_bibtex))
1. Cite as usual using `\cite{}`, `\citep{}`, `\citet{}`
1. To add page use `\cite[page number]{}`, `\citep[page number]{}`, `\citet[page number]{}`
1. Add bibliograpy at the end of the document
    ```latex
	\bibliography{bib_file}{}           % your bibliography .bib file, no need to type .bib at the end
	\bibliographystyle{cuharvard}       % specify referencing style
    ```

## Custom entry types
- website
- online lecture

For usage details see the [Wiki page](https://gitlab.com/tomasz.cichy98/cu-harvard-in-latex/-/wikis/Custom-BibTex-entries).

## [Support the project](https://www.paypal.me/cichyt)
https://www.paypal.me/cichyt

## External tutorials
- [How to use LaTeX](https://www.overleaf.com/learn/latex/Learn_LaTeX_in_30_minutes)
- [How to use BibTex](https://www.overleaf.com/learn/latex/bibliography_management_with_bibtex)
- How to work with `.bib` files:
    - any text or LaTeX editor should be able to edit them
    - you can also use a reference manager like:
        - [KBibTex](https://kde.org/applications/en/education/org.kde.kbibtex) (Linux)
        - [Zotero](https://www.zotero.org/)
        - [JabRef](https://www.jabref.org/)

Links:
- [CU Harvard Referencing Guide](https://curve.coventry.ac.uk/open/file/bdfb947c-9d43-48d3-8ec8-f511682e1dd1/1/The%20CU%20Guide%20to%20Referencing%20in%20Harvard%20Style.pdf)


